import React, { useState } from "react";
import Image from "next/image";

interface ToggleWhiteBoxProps {
  title: string;
  children: React.ReactNode;
}

const ToggleWhiteBox: React.FC<ToggleWhiteBoxProps> = ({ children, title }) => {
  const [expand, setExpand] = useState(false);

  return (
    <div
      className={`border border-solid border-payline-border-light rounded focus:bg-payline-dark focus:text-white text-payline-dark p-3 my-3 flex ${
        expand ? "bg-white" : "bg-transparent"
      }`}>
      <div
        className="pr-5 flex items-center"
        onClick={() => setExpand((ex) => !ex)}>
        {expand ? (
          <Image src="/images/svg/chevron-up.svg" width={14} height={18} />
        ) : (
          <Image src="/images/svg/chevron-down.svg" width={14} height={18} />
        )}
      </div>
      <div
        className="font-bold cursor-pointer"
        onClick={() => setExpand((ex) => !ex)}>
        <p>{title}</p>
        <div
          className={`w-full ${
            expand ? "visible h-max bg-white" : "invisible h-0 bg-transparent"
          }`}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default ToggleWhiteBox;
