import React from "react";
import Link from "next/link";
import Image from "next/image";
import {
  faLinkedin,
  faTwitter,
  faInstagram,
  faFacebook,
} from "@fortawesome/free-brands-svg-icons";
import FooterIcon from "./footerIcon";

const FooterBrandCol: React.FC<{ className: string }> = (props) => {
  const { className: classes } = props;
  return (
    <div className={classes}>
      <div className=" mb-4 lg:mb-14 2xl:mb-36 mt-12 lg:mt-0">
        <Image
          src="/images/payline-logo.svg"
          layout="fixed"
          height="32px"
          width="113.27px"
          alt="Payline Logo"
        />
      </div>
      <p className="text-lg text-payline-black mb-4 lg:mb-0">
        Flexible and Friendly Payment Processing Solutions, Tailored To Suit
        Your Needs
      </p>
      <div className="hidden lg:inline-flex py-3 w-full gap-4">
        <div className="font-opensans text-payline-black underline">
          <Link href="https://paylinedata.com/payline-terms-of-service/">
            Terms of Service
          </Link>
        </div>
        |
        <div className="font-opensans text-payline-black underline">
          <Link href="https://paylinedata.com/payline-privacy-policy/">
            Privacy Policy
          </Link>
        </div>
      </div>
      <div className="flex flex-row gap-3 mt-3 mb-6 lg:mb-0">
        <FooterIcon
          icon={faLinkedin}
          href="https://www.linkedin.com/company/payline-data"
        />
        <FooterIcon
          icon={faTwitter}
          href="https://twitter.com/hashtag/paylinedata"
        />
        <FooterIcon
          icon={faFacebook}
          href="https://www.facebook.com/Payline-Data-Services-178318544956/"
        />
        <FooterIcon
          icon={faInstagram}
          href="https://www.instagram.com/paylinedata/"
        />
      </div>
    </div>
  );
};

export default FooterBrandCol;
