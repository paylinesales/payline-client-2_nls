import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";
import Link from "@/components/UI/atoms/LinkComponent";

const TitleAndSupport: React.FC = () => {
  return (
    <div className="flex lg:flex-row flex-col justify-between my-6 lg:my-20 lg:items-center">
      <h1 className="text-4xl lg:text-5xl leading-normal lg:leading-tight font-bold text-payline-black order-1 lg:order-none mt-6 lg:mt-0">
        Interchange &
        <span className="ml-2 bg-payline-cta text-payline-white px-2">
          Pricing
        </span>
      </h1>
      <div className="flex items-center gap-5">
        <p className="font-bold text-payline-black hidden lg:block">
          Sales Team
        </p>
        <div className="flex items-center">
          <div className="border-2 border-payline-black rounded-full bg-sales-team w-20 h-20 bg-cover relative">
            <div className="w-4 h-4 rounded-full absolute z-1 bg-payline-glowing-green top-0 left-3/4" />
          </div>
          <Link
            href="#"
            className="bg-payline-dark font-bold text-payline-white py-2 px-6 border-2 border-payline-black rounded-r-3xl -ml-2 text-sm lg:text-lg hover:bg-payline-black transition-all">
            Schedule a Callback
            <FontAwesomeIcon icon={faAngleRight} className="ml-3 lg:ml-5" />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default TitleAndSupport;
