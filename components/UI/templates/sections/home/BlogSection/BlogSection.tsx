/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/require-default-props */
import React, { useState, useEffect } from "react";
import SectionTitle from "../../../../atoms/SectionTitle";
import BlogCategories from "./BlogCategories";
import PostsSlider from "./PostsSlider";

import client from "../../../../../../lib/apolloClient";
import { homeBlogQueries } from "../../../../../../queries/blog";

type blogData = {
  posts: { edges: any };
  categories: { edges: any };
};

const defaultBlogData = {
  posts: { edges: [] },
  categories: { edges: [] },
};

const BlogSection: React.FC = () => {
  const [blogPosts, setBlogPosts] = useState<blogData>(defaultBlogData);
  const [dataLoading, setDataLoading] = useState(false);

  useEffect(() => {
    const fetchPosts = async () => {
      const { loading, data } = await client.query({
        query: homeBlogQueries,
      });

      setDataLoading(loading);
      setBlogPosts(data);
    };

    fetchPosts();
  }, []);

  if (dataLoading) return <span>Loading</span>;

  const { posts, categories } = blogPosts || {};
  // Update Blog Background Image
  if (!posts || !categories) return <span>no posts</span>;

  return (
    <section
      id="blog-section"
      className="pt-24 pb-32 lg:pb-24 px-0 md:px-16 lg:px-20 xl:px-32">
      <div className="container mx-auto mb-12">
        <SectionTitle firstPartOfHeadline="The" highlighedText="Blog" />
      </div>
      <div className="w-full 2xl:w-4/5 xl:w-11/12 ml-auto relative -mr-0 md:-mr-16 lg:-mr-20 xl:-mr-32 pl-2">
        <BlogCategories
          className="w-full xl:w-10/12 mr-auto mb-10"
          categories={categories}
        />
        <PostsSlider posts={posts} />
      </div>
    </section>
  );
};

export default BlogSection;
