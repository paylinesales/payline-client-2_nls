import React from "react";
import SectionTitle from "components/UI/atoms/SectionTitle";
import PaylineDetail from "components/UI/atoms/PaylineDetail";
import { useBusinessCategory } from "context/BusinessCategoryContext";
import { PAYMENT_TYPES } from "../../../../../constants";

const { ONLINE } = PAYMENT_TYPES;
const details = [
  // Online
  [
    {
      title: "Mobile Readers",
      description:
        "Let chatbots generate leads, create help tickets, and chat across channels also anything else",
    },
    {
      title: "Countertop Readers",
      description:
        "Let chatbots generate leads, create help tickets, and chat across channels also anything else",
    },
    {
      title: "POS Systems",
      description:
        "Let chatbots generate leads, create help tickets, and chat across channels also anything else",
    },
  ],
  // In person
  [
    {
      title: "VT/Dashboard",
      description: "All your dashboard needs covered in one place… ",
    },
    {
      title: "Shopping cart integrations",
      description: "All your dashboard needs covered in one place… ",
    },
    {
      title: "Recurring Billing and invoicing",
      description: "All your dashboard needs covered in one place… ",
    },
  ],
];

const SolutionForYourNeeds: React.FC = () => {
  const { paymentType } = useBusinessCategory();

  // for as long as the payment type is on line then the business is online
  const isOnlineBusiness = paymentType === ONLINE;

  // Determine section partial classes based on the current business type query parameter
  const sectionClasses = ` bg-cover bg-no-repeat pt-24 pb-32 lg:pb-24 px-8 md:px-16 lg:px-20 xl:px-32 ${
    isOnlineBusiness ? "bg-sw" : "bg-hw"
  }`;
  const sectionShape = isOnlineBusiness
    ? "/images/svg/sw-shape.svg"
    : "/images/svg/hw-shape.svg";
  const sectionDetails = isOnlineBusiness ? details[0] : details[1];
  return (
    <section id="solution-for-your-needs-section" className={sectionClasses}>
      <div className="container mx-auto">
        <SectionTitle
          subtitle="The Options"
          firstPartOfHeadline={`${
            isOnlineBusiness ? "Software" : "Hardware"
          } to`}
          lastPartOfHeadline="suit your needs"
          highlighedText="suit"
        />
        <div className="flex flex-wrap mt-6 lg:mt-20">
          <div className="w-full lg:w-1/2 order-1 lg:order-none">
            {sectionDetails.map((detail) => (
              <PaylineDetail
                title={detail.title}
                className="border border-solid border-payline-blue rounded bg-transparent open:bg-white hw-details transition-colors duration-150"
                summaryClasses="text-2xl font-semibold">
                <p>{detail.description}</p>
              </PaylineDetail>
            ))}
          </div>
          <div className="w-full lg:w-1/2 md:px-14">
            <img src={sectionShape} className="w-full" />
          </div>
        </div>
      </div>
    </section>
  );
};

export default SolutionForYourNeeds;
