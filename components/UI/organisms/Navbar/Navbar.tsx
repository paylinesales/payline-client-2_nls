/* eslint-disable  react/require-default-props, react/no-unused-prop-types, react/forbid-prop-types */

import React, { useState, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faPhoneAlt } from "@fortawesome/free-solid-svg-icons";
import ButtonLink from "@/components/UI/molecules/buttonLink";
import NavLogo from "./NavLogo";
import NavMenu from "./NavMenu";
import NavToggler from "./NavToggler";
import DropdownMenu from "./DropdownMenu";

type NavBarProps = {
  tagLine?: string;
  navBar: any;
};

const mainMenu = [
  { name: "small business", href: "/small-business/online" },
  { name: "enterprise", href: "/enterprise/online" },
];
const rightMenu = [
  { name: "Solutions", href: "" },
  { name: "Pricing", href: "/pricing" },
  { name: "Blog", href: "/blog" },
];

const Navbar: React.FC<NavBarProps> = ({ navBar }) => {
  const [mobileMenuToggler, setMobileMenuToggler] = useState(false);
  const [toggleDropdownMenu, setToggleDropdownMenu] = useState(false);
  const mobileMenu = useRef<HTMLDivElement>(null);

  // eslint-disable-next-line no-console
  console.log(`This is the navBar:`, navBar);

  const toggleMobileMenuHandler = () => {
    if (mobileMenuToggler === false) {
      setMobileMenuToggler(true);
    } else {
      setMobileMenuToggler(false);
    }
    mobileMenu.current!.classList.toggle("hidden");
  };

  const toggleDropdownMenuHandler = () => {
    setToggleDropdownMenu((prevState) => {
      return !prevState;
    });
  };

  return (
    <nav id="mainNavbar" className="">
      <div className="container mx-auto py-4">
        <div className="flex justify-between items-center px-8">
          <div className="flex space-x-7">
            {/* Navbar Logo */}
            <NavLogo />
            {/* Primary/Left-Aligned Menu */}
            <NavMenu
              menu={mainMenu}
              className="hidden xl:flex items-center gap-6"
            />
          </div>
          {/* Secondary/Right-Aligned Menu */}
          <NavMenu
            menu={rightMenu}
            className="hidden xl:flex items-center gap-6"
            toggleDropdownMenu={toggleDropdownMenuHandler}
            isDropdownMenu={toggleDropdownMenu}>
            <div className="flex flex-wrap gap-3 md:flex-nowrap items-center pl-7">
              <FontAwesomeIcon icon={faPhoneAlt} />
              <span>(312) 815-2048</span>
            </div>
            <div className="button hidden xl:block">
              <ButtonLink
                className="text-sm font-bold md:py-2 px-6 font-sans bg-payline-dark text-payline-white hover:bg-payline-black transition-all"
                href="https://paylinedata.com/apply-now-ue/">
                Apply Now
                <FontAwesomeIcon icon={faAngleRight} className="ml-5" />
              </ButtonLink>
            </div>
          </NavMenu>
          {/* Mobile menu button */}
          <NavToggler toggleMobileMenuHandler={toggleMobileMenuHandler} />
        </div>
        {/* Mobile menu */}
        <div
          className="hidden mobile-menu bg-payline-cta px-8 py-5 text-white"
          ref={mobileMenu}>
          <NavMenu menu={mainMenu} className="flex flex-col gap-4" />
          <div className="h-px w-full bg-payline-disabled my-4" />
          <NavMenu menu={rightMenu} className="flex flex-col gap-4">
            <div className="h-px w-full bg-payline-disabled" />
            <div className="button block">
              <ButtonLink
                className="text-sm font-bold py-2 px-6 font-sans bg-payline-black xl:bg-payline-dark text-payline-white"
                href="https://paylinedata.com/apply-now-ue/">
                Apply Now
                <FontAwesomeIcon icon={faAngleRight} className="ml-5" />
              </ButtonLink>
            </div>
          </NavMenu>
        </div>
      </div>
      {toggleDropdownMenu && (
        <DropdownMenu
          items={navBar[2]}
          onMouseLeave={toggleDropdownMenuHandler}
        />
      )}
    </nav>
  );
};

export default Navbar;

// ToDo: Organize menubar in ACF to be more modular
