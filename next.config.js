module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["images.unsplash.com", "paylinelocal.wpengine.com"],
  },
  async redirects() {
    return [
      {
        source: "/visa-interchange-rates",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/credit-card-processing-pricing",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/american-express-interchange-rates",
        destination: "/pricing#american-express",
        permanent: false,
      },
      {
        source: "/mastercard-interchange-rates",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/discover-interchange-rates#discover-card",
        destination: "/pricing",
        permanent: false,
      },
      {
        source: "/",
        destination: "/small-business/online",
        permanent: true,
      },
    ];
  },
};
