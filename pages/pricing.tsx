/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import client from "lib/apolloClient";
import * as React from "react";
import BlogLayout from "@/components/UI/layouts/BlogLayout";
import CalculateYourMonthlyCostSectionPricing from "@/components/UI/templates/sections/home/CalculateYourMonthlyCost/CalculateYourMonthlyCostSection";
import InterchangePricingSection from "@/components/UI/templates/sections/pricing/InterchangePricingSection";
import InterchangeRate from "@/components/UI/templates/sections/pricing/InterchangeRate";
import FAQSection from "@/components/UI/templates/sections/home/FAQSection";
import FairSquarePriceComparison from "@/components/UI/templates/sections/pricing/FairSquarePriceComparison";
import interchangeQuery from "../queries/pricing-page/Interchange";
import websiteCopyQuery from "../queries/websiteCopy";

interface PricingProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  interchangeData: React.ReactNode | any;
  // eslint-disable-next-line react/no-unused-prop-types
  interchangeRate: React.ReactNode;
  websiteCopyData: string;
}

const Pricing: React.FC<PricingProps> = ({
  interchangeData,
  websiteCopyData,
}) => {
  let websiteCopyDataParsed = JSON.parse(websiteCopyData);
  websiteCopyDataParsed = websiteCopyDataParsed.data.websiteCopy;
  const { calculatorSection, faqSection } = websiteCopyDataParsed;
  return (
    <BlogLayout>
      <InterchangePricingSection />
      <CalculateYourMonthlyCostSectionPricing
        calculatorData={calculatorSection}
        className="pt-56"
      />
      <InterchangeRate interchangeData={interchangeData} />
      <FairSquarePriceComparison />
      <FAQSection faqData={faqSection} />
    </BlogLayout>
  );
};

export async function getServerSideProps() {
  const interchange = await client.query({
    query: interchangeQuery,
  });
  const websiteCopy = await client.query({
    query: websiteCopyQuery,
  });

  const interchangeData = JSON.stringify(interchange);
  const websiteCopyData = JSON.stringify(websiteCopy);

  return {
    props: { interchangeData, websiteCopyData },
  };
}

export default Pricing;
