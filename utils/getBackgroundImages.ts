import { BUSINESS_TYPES, PAYMENT_TYPES } from "../constants";

const { SMALL_BUSINESS, ENTERPRISE } = BUSINESS_TYPES;
const { ONLINE, IN_PERSON } = PAYMENT_TYPES;
export type businessCategoryType = {
  businessType: string;
  paymentType: string;
};

type backgroundUrlsType = {
  heroUrl: string;
  revenueCartoonUUrl: string;
  calculatorBgURL: string;
  suitYourNeedsBgUrl: string;
  FAQBgUrl: string;
};

const getPageBackgroundImages = (
  params: businessCategoryType,
): backgroundUrlsType => {
  // const { businessCategory } = params;
  // const {
  //   isSmallBusinessOnline,
  //   isSmallBusinessInperson,
  //   isEnterpriseBusinessOnline,
  //   isEnterpriseBusinessInperson,
  // } = params || {};

  const { businessType, paymentType } = params;

  // default values based on smallbusiness online
  let backgroundUrls = {
    heroUrl: "/images/small-business-software-bg.png",
    revenueCartoonUUrl: "",
    calculatorBgURL: "",
    suitYourNeedsBgUrl: "",
    FAQBgUrl: "",
  };

  if (
    Boolean(businessType === SMALL_BUSINESS) &&
    Boolean(paymentType === IN_PERSON)
  ) {
    backgroundUrls = {
      ...backgroundUrls,
      heroUrl: "/images/cashier-woman-with-background.png",
    };
  }
  if (
    Boolean(businessType === SMALL_BUSINESS) &&
    Boolean(paymentType === ONLINE)
  ) {
    backgroundUrls = {
      ...backgroundUrls,
      heroUrl: "/images/small-business-software-bg.png",
    };
  }

  if (Boolean(businessType === ENTERPRISE) && Boolean(paymentType === ONLINE)) {
    backgroundUrls = {
      ...backgroundUrls,
      heroUrl: "/images/blue-cashier-bg.png",
    };
  }

  if (
    Boolean(businessType === ENTERPRISE) &&
    Boolean(paymentType === IN_PERSON)
  ) {
    backgroundUrls = {
      ...backgroundUrls,
      heroUrl: "/images/enterprise-hardware-bg.png",
    };
  }

  return backgroundUrls;
};

export { getPageBackgroundImages };
