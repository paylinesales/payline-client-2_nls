import { gql } from "@apollo/client";

const categoriesQuery = gql`
  query categoriesQuery {
    categories(first: 10) {
      edges {
        node {
          name
          slug
        }
      }
    }
  }
`;

export const allCategoryQuery = gql`
  query allCategoryQuery {
    categories {
      edges {
        node {
          name
          slug
          posts {
            edges {
              node {
                slug
              }
            }
          }
        }
      }
    }
  }
`;

export default categoriesQuery;
