const mod = (n, m) => {
  return ((n % m) + m) % m;
};

export default mod;
